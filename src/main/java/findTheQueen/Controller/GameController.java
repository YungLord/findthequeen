package findTheQueen.Controller;
import java.util.*;



import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;



import org.codehaus.jackson.map.ObjectMapper;


import entitiy.Player;




@Controller
public class GameController {
  ObjectMapper mapper = new ObjectMapper();
  List<Integer> entry  = new ArrayList<Integer>();
  List<Player> players  = new ArrayList<Player>();
  
  Random rand = new Random();
  String role1 = "Dealer";
  String role2 = "Spotter";
  int count = 0;
  int click = 0;
  int choiceCount = 0;
  int firstChoice =0;
  @MessageMapping("/compare")
  @SendTo("/game/result")
  public int compare(int choice ) throws Exception {	  
    Thread.sleep(1000); // simulated delay    
    int result = -1;    
    choiceCount++;
    if (choiceCount == 1) {
    	System.out.print("Player1 choice: "+choice+"\r\n");    	
    	System.out.print("Cannot compare yet \r\n");  
    	firstChoice = choice;	
    }else if (choiceCount == 2) {
    	if(choice == firstChoice) {
    		System.out.print("Player1 choice:"+firstChoice+"\r\n");    		
    		System.out.print("Player2 choice: "+choice+"\r\n");    	
    		System.out.print("Spotter won \r\n");
    		result = 1;    		
    	}else {
    		System.out.print("Player1 choice:  "+firstChoice+"\r\n");    		
    		System.out.print("Player2 choice: "+choice+"\r\n");   
    		System.out.print("Dealer won \r\n");
    		result = 0;    	
    	}    	 
        choiceCount = 0;
        entry.clear();  	  
    }	
    return result; 
  }

  
  @MessageMapping("/clickSend")
  @SendTo("/game/clickCount")
  public int trackClick(int num ) throws Exception {	  
    Thread.sleep(1000); // simulated delay
    int result = -1;
    click++;    
    if (click== 1)
    {
    	result = 1;
    }else if (click== 2){
    	result = 2;
    	click = 0;
    }else {
    	System.out.print("Click out of bounds \r\n");
    }
   return result;
  }

  
  
  @MessageMapping("/log")
  @SendTo("/game/play")
  public  List<Player> add( String playerString ) throws Exception {	 
	  Thread.sleep(1000);
	  System.out.print("Player String: "+playerString+"\r\n");
		   List<Player> result  = new ArrayList<Player>();
			Player player = new Player();			
			player = mapper.readValue(playerString, Player.class);			  	  		         				    		        
			System.out.print("Player String: "+playerString+"\r\n");
			System.out.print("Player object: "+player+"\r\n");
			System.out.print("This count should be 0 "+count+"\r\n");			
			count++;    
		    if (count == 1)
		    {    	
		        int num = rand.nextInt(2) ;   
		        if (num == 0)
		        {
		        	player.setRole(role1);		    			    	
			    	players.add(player);		    	
			    	result = players;
		        }else {
		        	player.setRole(role2);		    			    	
			    	players.add(player);		    	
			    	result = players;
		        }
		    	
		    	
		    }else if (count == 2)
		    	{			    	
			    	if (players.get(0).getRole() == role1)
			    	{
			    		player.setRole(role2);   			    		
			    	   	players.add(player);			    	   					
			    	}else {
			    		player.setRole(role1);			    		
			    	 	players.add(player);			    	   								    	 	
			    	}			    	
			    	result.addAll(players);		
			    	System.out.print("Two players in match \r\n");	
			    	players.clear();			    				    	
			    	count = 0;			    				    	
		        }else {
		        	
		        	System.out.print("Array Out of bounds\r\n");
		        }  		     
		    return result;
		   
  }
  
  
 
  
  
  
  
  
  
}
