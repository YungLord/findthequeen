package entitiy;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Player {
private String role;
private String username;
private String client;

public Player() {	
	this.username = "default";
	this.role = "default role";
	this.client = null;
}
  
  public Player(@JsonProperty("role") String role, @JsonProperty("username")String username ,  @JsonProperty("client")String client) {
	super();
	this.username = username;
	this.role = role;
	this.client = client;
}

public String getUsername() {
	return username;
}

public void setUsername(String username) {
	this.username = username;
}

public String getRole() {
	return role;
}


public void setRole(String role) {
	this.role = role;
}

public String getClient() {
	return client;
}

public void setClient(String client) {
	this.client = client;
}

@Override
public String toString() {
	return "Player [role=" + role + ", username=" + username + ", client=" + client + "]";
}

  
}